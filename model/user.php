<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . '/model/basicModel.php';

class User extends BasicModel
{

    static function isValidToken($user_id, $token)
    {
        $result = false;

        $tablename = "users";
        $sql = "SELECT TOKEN FROM $tablename WHERE USER_ID = $user_id";
        $queryResult = mysql_query($sql);
        while ($row = mysql_fetch_array($queryResult))
        {
            extract($row);
            $result = $token == $TOKEN;
        }

        return $result;
    }

    static function updateLastFriendsOperationTimestamp($user_id)
    {
        $tablename = "friends_operation_status";
        $timestamp = time();

        mysql_query("DELETE FROM $tablename WHERE USER_ID = $user_id");
        mysql_query("INSERT INTO $tablename (USER_ID, TIMESTAMP) VALUES ($user_id, $timestamp)");
    }

    static function lastFriendsOperationTimestamp($user_id)
    {
        $result = 0;

        $tablename = "friends_operation_status";
        $sql = "SELECT TIMESTAMP FROM $tablename WHERE USER_ID = $user_id";
        $queryResult = mysql_query($sql);
        while ($row = mysql_fetch_array($queryResult))
        {
            extract($row);
            $result = $TIMESTAMP;
        }

        return $result;
    }

    static function updateLastEventsOperationTimestamp($user_id)
    {
        $tablename = "events_operation_status";
        $timestamp = time();

        mysql_query("DELETE FROM $tablename WHERE USER_ID = $user_id");
        mysql_query("INSERT INTO $tablename (USER_ID, TIMESTAMP) VALUES ($user_id, $timestamp)");
    }

    static function lastEventsOperationTimestamp($user_id)
    {
        $result = 0;

        $tablename = "events_operation_status";
        $sql = "SELECT TIMESTAMP FROM $tablename WHERE USER_ID = $user_id";
        $queryResult = mysql_query($sql);
        while ($row = mysql_fetch_array($queryResult))
        {
            extract($row);
            $result = $TIMESTAMP;
        }

        return $result;
    }

    static function updateLastRostersOperationTimestamp($user_id)
    {
        $tablename = "rosters_operation_status";
        $timestamp = time();

        mysql_query("DELETE FROM $tablename WHERE USER_ID = $user_id");
        mysql_query("INSERT INTO $tablename (USER_ID, TIMESTAMP) VALUES ($user_id, $timestamp)");
    }

    static function lastRostersOperationTimestamp($user_id)
    {
        $result = 0;

        $tablename = "rosters_operation_status";
        $sql = "SELECT TIMESTAMP FROM $tablename WHERE USER_ID = $user_id";
        $queryResult = mysql_query($sql);
        while ($row = mysql_fetch_array($queryResult))
        {
            extract($row);
            $result = $TIMESTAMP;
        }

        return $result;
    }

    static function updateLastShiftsOperationTimestamp($user_id)
    {
        $tablename = "shifts_operation_status";
        $timestamp = time();

        mysql_query("DELETE FROM $tablename WHERE USER_ID = $user_id");
        mysql_query("INSERT INTO $tablename (USER_ID, TIMESTAMP) VALUES ($user_id, $timestamp)");
    }

    static function lastShiftsOperationTimestamp($user_id)
    {
        $result = 0;

        $tablename = "shifts_operation_status";
        $sql = "SELECT TIMESTAMP FROM $tablename WHERE USER_ID = $user_id";
        $queryResult = mysql_query($sql);
        while ($row = mysql_fetch_array($queryResult))
        {
            extract($row);
            $result = $TIMESTAMP;
        }

        return $result;
    }

    static function isEmailUnique($email)
    {
        $result = 0;

        $tablename = "users";
        $sql = "SELECT COUNT(*) AS COUNT FROM $tablename WHERE EMAIL = '$email'";
        $queryResult = mysql_query($sql);
        while ($row = mysql_fetch_array($queryResult))
        {
            extract($row);
            $result = $COUNT;
        }

        return $result <= 0;
    }

}
