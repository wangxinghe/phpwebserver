<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . '/model/basicModel.php';

class UserShiftsState extends BasicModel
{

    static function isUpdatedSince($user_id, $timestamp)
    {
        $tablename = "user_shifts_states";
        $query = mysql_query("SELECT COUNT(*) AS RESULT FROM $tablename WHERE USER_ID = $user_id AND TIMESTAMP > $timestamp");

        while ($row = mysql_fetch_array($query))
        {
            extract($row);
            return (Integer) $RESULT > 0;
        }
    }

    static function update($user_id)
    {
        $tablename = "user_shifts_states";
        $timestamp = time();

        mysql_query("DELETE FROM $tablename WHERE USER_ID = $user_id");
        mysql_query("INSERT INTO $tablename (USER_ID, TIMESTAMP) VALUES ($user_id, $timestamp)");
    }

    static function getTimestamp($user_id)
    {
        $tablename = "user_shifts_states";
        $query = mysql_query("SELECT TIMESTAMP FROM $tablename WHERE USER_ID = $user_id");

        while ($row = mysql_fetch_array($query))
        {
            extract($row);
            return (Double) $TIMESTAMP;
        }
    }

}
