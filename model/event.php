<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

require_once $_SERVER["DOCUMENT_ROOT"] . '/model/basicModel.php';

class Event extends BasicModel
{

    var $user_id = -1;
    var $title = "";
    var $location = "";
    var $friends = array();
    var $all_day = 0;
    var $start_time = 0.0;
    var $end_time = 0.0;
    var $repeat = 0;
    var $type = "";
    var $alert = 0;
    var $enabled = 1;

}
