<?php

/*
 * API: /user/updateInfo.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "first_name":"Ricol",
 *       "last_name":"Wang",
 *       "email":"ricol.wang@appscore.com.au",
 *       "tel":"0423584908",
 *       "location":"51 Cardigan Carlton 3053 VIC"
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/user.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $first_name = $input->first_name;
    $last_name = $input->last_name;
    $email = $input->email;
    $tel = $input->tel;
    $location = $input->location;

    $timestamp = time();
    $tablename = "users";

    //check email to make sure it is unique!
    if (User::isEmailUnique($email))
    {
        //if doesn't exist, return succeed

        $sql = "UPDATE $tablename SET FIRST_NAME = '$first_name', LAST_NAME = '$last_name', EMAIL = '$email', TEL = '$tel', LOCATION = '$location', TIMESTAMP = $timestamp WHERE USER_ID = $user_id";

        mysql_query($sql);

        $result = array("status" => 0,
            "message" => "",
            "timestamp" => $timestamp);

        header('Content-type: application/json');
        echo(json_encode($result));
    }else
    {
        $result = array("status" => 1,
            "message" => "Email duplicated!",
            "timestamp" => $timestamp);

        header('Content-type: application/json');
        echo(json_encode($result));
    }

    closeDB();
}
?>