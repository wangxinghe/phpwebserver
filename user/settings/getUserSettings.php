<?php

/*
 * API: /user/settings/getUserSettings.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "lastUpdated":1447828080.00074
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *       "timestamp":1447828080.00074,
 *       "settings": ""
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate((Integer) $input->user_id, (String) $input->token);

    $user_id = $input->user_id;
    $lastUpdated = $input->lastUpdated;

    // check the email address in the users table
    $tablename = "users_settings";
    $sql = "SELECT SETTINGS FROM $tablename WHERE USER_ID = $user_id AND TIMESTAMP > $lastUpdated";
    $queryResult = mysql_query($sql);

    $timestamp = time();

    $value = "";
    while ($row = mysql_fetch_array($queryResult))
    {
        extract($row);
        $value = $SETTINGS;
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp,
        "settings" => $value);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>