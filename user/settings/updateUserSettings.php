<?php

/*
 * API: /user/settings/getUserSettings.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "settings":""
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *       "timestamp":1447828080.00074
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $settings = $input->settings;

    $timestamp = time();

    // check the email address in the users table
    $tablename = "users_settings";
    mysql_query("DELETE FROM $tablename WHERE USER_ID = $user_id");
    $sql = "INSERT INTO $tablename (USER_ID, SETTINGS, TIMESTAMP) VALUES ($user_id, '$settings', $timestamp)";
    $queryResult = mysql_query($sql);

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>