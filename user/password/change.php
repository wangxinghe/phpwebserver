<?php

/*
 * API: /user/password/change.php
 * 
 * Input:
 * 
 * {
 *       "user_email": "ricol.wang@appscore.com.au",
 *       "token":"!@#$%^&*()", 
 *       "old_password":"star1983",
 *       "new_password":"ricol1983"
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    openDB();

    $result = '{
            "status": 0,
            "message": "",
            "timestamp":1447828080.00074
        }';

    header('Content-type: application/json');
    echo($result);

    closeDB();
}
?>
