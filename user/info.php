<?php

/*
 * API: /user/info.php
 * 
 * Input:
 * 
 * {
 *       "user_id":1,
 *       "token": "123123123123123123123",
 *       "lastUpdated":1447828080.00074,
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *      "timestamp":1447828080.00074,
 *       "user":{  
 *             "first_name":"Ricol",
 *             "last_name":"Wang",
 *             "email":"ricol.wang@appscore.com.au",
 *             "tel":"0423584908",
 *             "location":"51 Cardigan Carlton 3053 VIC",
 *             "isFacebook":0
 *            }
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $lastUpdated = $input->lastUpdated;

    // check the email address in the users table
    $tablename = "users";
    $sql = "SELECT * FROM $tablename WHERE USER_ID = $user_id";
    $queryResult = mysql_query($sql);

    $details = array();
    $timestamp = time();

    while ($row = mysql_fetch_array($queryResult))
    {
        extract($row);

        $details["user_id"] = (Integer) $USER_ID;
        $details["first_name"] = $FIRST_NAME;
        $details["last_name"] = $LAST_NAME;
        $details["email"] = $EMAIL;
        $details["tel"] = $TEL;
        $details["location"] = $LOCATION;
        $details["profile"] = $PROFILE;
        $details["isFacebook"] = (Integer) $IS_FACEBOOK;
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp,
        "user" => $details);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>