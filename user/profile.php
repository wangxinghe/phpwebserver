<?php

/*
 * API: /user/profile.php
 * 
 * Input:
 * 
 * {
 *       "user_id":1,
 *       "token": "123123123123123123123",
 *       "lastUpdated":1447828080.00074,
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *       "timestamp":1447828080.00074,
 *       "profile":"123123123123"
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;

    // check the email address in the users table
    $tablename = "users";
    $sql = "SELECT PROFILE FROM $tablename WHERE USER_ID = $user_id";
    $queryResult = mysql_query($sql);

    $profile = "";
    $timestamp = time();

    while ($row = mysql_fetch_array($queryResult))
    {
        extract($row);

        $profile = $PROFILE;
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp,
        "profile" => $profile);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>