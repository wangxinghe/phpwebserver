<?php

/*
 * API: /user/updateProfile.php
 * 
 * Input:
 * 
 * {
 *       "user_id":1,
 *       "token": "123123123123123123123",
 *       "lastUpdated":1447828080.00074,
 *       "profile_image":"1234567890"
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *       "timestamp":1447828080.00074
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $profile_image = $input->profile_image;

    $timestamp = time();
    // check the email address in the users table
    $tablename = "users";
    $sql = "UPDATE $tablename SET PROFILE = '$profile_image', TIMESTAMP = $timestamp WHERE USER_ID = $user_id";
    $queryResult = mysql_query($sql);

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>