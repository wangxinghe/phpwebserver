<?php

/*
 * API: /user/rosters/updateRoster.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "roster":{  
 *          "id": 1,
 *          "title":"holiday",
 *          "type":"",
 *          "start_time":1447828080.00074,
 *          "end_time":1447828080.00074,
 *          "end_time_required":1,
 *          "shifts": {
 *              1447828080.00074: 0,
 *              1447818080.00074: 1,
 *              1447828080.00074: 0,
 *              1447838080.00074: 2,
 *              1447848080.00074: 0
 *          }
 *      }
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/roster.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $roster = new Roster();
    $roster->user_id = (Integer) $input->user_id;
    $roster->id = (Integer) $input->roster->id;
    $roster->title = $input->roster->title;
    $roster->type = $input->roster->type;
    $roster->start_time = (Double) $input->roster->start_time;
    $roster->end_time = (Double) $input->roster->end_time;
    $roster->end_time_required = (Integer) $input->roster->end_time_required;
    $timestamp = time();
    $roster->timestamp = $timestamp;
    $roster->shifts = $input->roster->shifts;

    $tablename = "rosters";

    $sql = "UPDATE $tablename SET USER_ID = $roster->user_id, TITLE = '$roster->title', TYPE = '$roster->type', START_TIME = $roster->start_time, END_TIME = $roster->end_time, END_TIME_REQUIRED = $roster->end_time_required, TIMESTAMP = $roster->timestamp WHERE ROSTER_ID = $roster->id";
    $queryResult = mysql_query($sql);

    $tablename = "rosters_shifts";

    //delete old shifts for the roster
    $sql = "DELETE FROM $tablename WHERE ROSTER_ID = $roster->id";
    $queryResult = mysql_query($sql);

    //insert new shifts
    foreach ($roster->shifts as $date => $shift_id)
    {
        $sql = "INSERT INTO $tablename (ROSTER_ID, DATE, SHIFT_ID, TIMESTAMP) "
                . "VALUES ($roster->id, $date, $shift_id, $roster->timestamp)";
        $queryResult = mysql_query($sql);
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}