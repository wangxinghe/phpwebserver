<?php

/*
 * API: /user/rosters/addRoster.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "roster":{  
 *          "title":"holiday",
 *          "type":"",
 *          "start_time":1447828080.00074,
 *          "end_time":1447828080.00074,
 *          "end_time_required":1,
 *          "shiftCycles": [
 *              {   "indexId": 0,
 *                  "daysOn": 5,
 *                  "daysOff": 2
 *              },
 *              {
 *                  "indexId"; 1
 *                  "daysOn": 3,
 *                  "daysOff": 1
 *              },
 *          ]
 *      }
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 *      "roster_id":1
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/roster.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $roster = new Roster();
    $roster->user_id = (Integer) $input->user_id;
    $roster->title = $input->roster->title;
    $roster->type = $input->roster->type;
    $roster->start_time = (Double) $input->roster->start_time;
    $roster->end_time = (Double) $input->roster->end_time;
    $roster->end_time_required = (Integer) $input->roster->end_time_required;
    $timestamp = time();
    $roster->timestamp = $timestamp;
    $roster->shiftCycles = $input->roster->shiftCycles;

    $tablename = "rosters";

    $sql = "INSERT INTO $tablename (USER_ID, TITLE, TYPE, START_TIME, END_TIME, END_TIME_REQUIRED, TIMESTAMP) "
            . "VALUES ($roster->user_id, '$roster->title', '$roster->type', $roster->start_time, $roster->end_time, $roster->end_time_required, $roster->timestamp)";
    $queryResult = mysql_query($sql);
    $roster->id = mysql_insert_id();

    $tablename = "shiftCycles";
    foreach ($roster->shiftCycles as $aCycle)
    {
        $indexId = (Integer) $aCycle->indexId;
        $daysOn = (Integer) $aCycle->daysOn;
        $daysOff = (Integer) $aCycle->daysOff;

        $sql = "INSERT INTO $tablename (ROSTER_ID, INDEX_ID, DAYS_ON, DAYS_OFF, TIMESTAMP) "
                . "VALUES ($roster->id, $indexId, $daysOn, $daysOff, $roster->timestamp)";
        $queryResult = mysql_query($sql);
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp,
        "roster_id" => $roster->id);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}