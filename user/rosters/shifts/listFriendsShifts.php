<?php

/*
 * API: /user/rosters/shifts/listFriendsShifts.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "friends": {
 *          "1": 1447828080.00074,
 *          "2": 1447828080.00074,
 *        }
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *       "timestamp":1447828080.00074,
 *       "friendsShifts":{
 *          "1": {
 *             "timestamp": 0,
 *             "shifts": [
 *                  {
 *                      "id": 1,
 *                      "title": "",
 *                      "start_time": 0,
 *                      "end_time": 0,
 *                      "timestamp": 1447828080.00074
 *                  },
 *                  {
 *                      "id": 2,
 *                      "title": "",
 *                      "start_time": 0,
 *                      "end_time": 0,
 *                      "timestamp": 1447828080.00074
 *                  },
 *              ]
 *          },
 *          "2": {},
 *          "3": {}
 *      }
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/userShiftsState.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDB();
    openDBAndValidate($input->user_id, $input->token);

    $friends = $input->friends;

    $tablename = "shifts";

    $friendsShifts = array();

    foreach ($friends as $friend_id => $lastTimeStamp)
    {
        $updated = UserShiftsState::isUpdatedSince($friend_id, $lastTimeStamp);

        if ($updated)
        {
            $queryResult = mysql_query("SELECT * FROM $tablename WHERE USER_ID = $friend_id");

            $shiftsForAFriend = array();
            $shiftsForAFriend["timestamp"] = UserShiftsState::getTimestamp($friend_id);

            $shiftsForUser = array();
            while ($rowShifts = mysql_fetch_array($queryResult))
            {
                extract($rowShifts);

                $shiftDetails = array();

                $shift_id = $SHIFT_ID;

                $shiftDetails["id"] = (Integer) $SHIFT_ID;
                $shiftDetails["title"] = $TITLE;
                $shiftDetails["start_time"] = (Double) $START_TIME;
                $shiftDetails["end_time"] = (Double) $END_TIME;
                $shiftDetails["timestamp"] = (Double) $TIMESTAMP;

                array_push($shiftsForUser, $shiftDetails);
            }

            $shiftsForAFriend["shifts"] = $shiftsForUser;
            $friendsShifts[$friend_id] = $shiftsForAFriend;
        }
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => time(),
        "friendsShifts" => $friendsShifts);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}