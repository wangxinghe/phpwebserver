<?php

/*
 * API: /user/rosters/shifts/removeShift.php
 * 
 * Input: 
 * 
 * {
 *       "user_id": 0,
 *       "token": "123123123123123123123",
 *       "shift_id": 0
 * }
 * 
 * Output:
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp": 1447828080.00074
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/userShiftsState.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $shift_id = (Integer) $input->shift_id;
    $user_id = (Integer) $input->user_id;

    $tablename = "shifts";

    $sql = "DELETE FROM $tablename WHERE SHIFT_ID = $shift_id";
    $queryResult = mysql_query($sql);

    User::updateLastShiftsOperationTimestamp($user_id);
    UserShiftsState::update($user_id);

    $timestamp = time();
    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}