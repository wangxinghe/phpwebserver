<?php

/*
 * API: /user/rosters/shifts/listShifts.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "lastUpdated":1447828080.00074
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *       "timestamp":1447828080.00074,
 *       "isAll": 0,
 *       "shifts": {
 *             "1": {
 *                      "user_id": 0,
 *                      "roster_id": 0,
 *                      "title": "",
 *                      "type": "",
 *                      "desc": "",
 *                      "start_time": 0,
 *                      "end_time": 0,
 *                      "timestamp": 1447828080.00074
 *                },
 *             "2": {},
 *             "3": {},
 *             "4": {},
 *             "5": {}
 *       }
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $lastUpdated = $input->lastUpdated;

    $isAll = (Integer) $lastUpdated < (Integer) User::lastShiftsOperationTimestamp($user_id);

    $tablename = "shifts";
    $sql = "SELECT DISTINCT SHIFT_ID FROM $tablename WHERE USER_ID = $user_id";

    if (!$isAll)
    {
        $sql = $sql . " AND TIMESTAMP > $lastUpdated";
    }

    $queryResult = mysql_query($sql);

    $shifts = array();
    $timestamp = time();

    while ($row = mysql_fetch_array($queryResult))
    {
        extract($row);

        $shift_id = $SHIFT_ID;

        $tablenameShifts = "shifts";
        $queryResultShifts = mysql_query("SELECT * FROM $tablenameShifts WHERE SHIFT_ID = $shift_id");

        $shiftDetails = array();
        while ($rowShifts = mysql_fetch_array($queryResultShifts))
        {
            extract($rowShifts);
            $shiftDetails["user_id"] = (Integer) $USER_ID;
            $shiftDetails["roster_id"] = (Integer) $ROSTER_ID;
            $shiftDetails["title"] = $TITLE;
            $shiftDetails["type"] = $TYPE;
            $shiftDetails["desc"] = $DESCRIPTION;
            $shiftDetails["start_time"] = (Double) $START_TIME;
            $shiftDetails["end_time"] = (Double) $END_TIME;
            $shiftDetails["timestamp"] = (Double) $TIMESTAMP;
        }

        $shifts[$shift_id] = $shiftDetails;
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp,
        "isAll" => $isAll,
        "shifts" => $shifts);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}