<?php

/*
 * API: /user/rosters/shifts/addShifts.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "roster_id": 1,
 *       "shifts":[
 *          {  
 *              "title":"holiday",
 *              "type":"",
 *              "desc":"",
 *              "start_time":1447828080.00074,
 *              "end_time":1447828080.00074
 *          },
 *          {  
 *              "title":"holiday",
 *              "type":"",
 *              "desc":"",
 *              "start_time":1447828080.00074,
 *              "end_time":1447828080.00074
 *          }
 *       ]
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/shift.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/userShiftsState.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $shifts = $input->shifts;
    $timestamp = time();
    $tablename = "shifts";

    mysql_query("START TRANSACTION");
    foreach ($shifts as $aNewShift)
    {
        $shift = new Shift();
        $shift->user_id = (Integer) $user_id;
        $shift->roster_id = (Integer) $input->roster_id;
        $shift->title = $aNewShift->title;
        $shift->type = $aNewShift->type;
        $shift->desc = $aNewShift->desc;
        $shift->start_time = (Double) $aNewShift->start_time;
        $shift->end_time = (Double) $aNewShift->end_time;
        $shift->timestamp = $timestamp;

        $sql = "INSERT INTO $tablename (USER_ID, ROSTER_ID, TITLE, TYPE, DESCRIPTION, START_TIME, END_TIME, TIMESTAMP) "
                . "VALUES ($shift->user_id, $shift->roster_id, '$shift->title', '$shift->type', '$shift->desc', $shift->start_time, $shift->end_time, $shift->timestamp)";
        $queryResult = mysql_query($sql);
    }
    mysql_query("COMMIT");

    UserShiftsState::update($user_id);

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}