<?php

/*
 * API: /user/rosters/removeRoster.php
 * 
 * Input: 
 * 
 * {
 *      "user_id": 0,
 *       "token": "123123123123123123123",
 *      "roster_id": 0
 * }
 * 
 * Output:
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp": 1447828080.00074
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/userShiftsState.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/shiftCycle.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = (Integer) $input->user_id;
    $roster_id = (Integer) $input->roster_id;

    $tablename = "rosters";

    $sql = "DELETE FROM $tablename WHERE ROSTER_ID = $roster_id";
    $queryResult = mysql_query($sql);

    ShiftCycle::deleteShiftCyclesForRoster($roster_id);

    User::updateLastRostersOperationTimestamp($user_id);

    $tablename = "shifts";
    $sql = "DELETE FROM $tablename WHERE ROSTER_ID = $roster_id";
    $queryResult = mysql_query($sql);

    User::updateLastShiftsOperationTimestamp($user_id);

    UserShiftsState::update($user_id);

    $timestamp = time();
    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}