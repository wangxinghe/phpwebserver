<?php

/*
 * API: /user/rosters/listRosters.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "lastUpdated":1447828080.00074
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *       "timestamp":1447828080.00074,
 *       "isAll": 0,
 *       "rosters": {
 *             "1": {
 *                  "user_id": 0,
 *                  "title": "",
 *                  "type": "",
 *                  "start_time": 0,
 *                  "end_time": 0,
 *                  "end_time_required": 1
 *                  "shiftCycles": [
 *                      {   "indexId": 0,
 *                          "daysOn": 5,
 *                          "daysOff": 2,
 *                          "timestamp": 0
 *                      },
 *                      {
 *                          "indexId"; 1
 *                          "daysOn": 3,
 *                          "daysOff": 1,
 *                          "timestamp": 0
 *                      },
 *                  ],
 *                  "timestamp": 1447828080.00074
 *              },
 *             "2": {},
 *             "3": {},
 *             "4": {},
 *             "5": {}
 *       }
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = (Integer) $input->user_id;
    $lastUpdated = $input->lastUpdated;

    $isAll = (Integer) $lastUpdated < (Integer) User::lastRostersOperationTimestamp($user_id);

    $tablename = "rosters";
    $sql = "SELECT DISTINCT ROSTER_ID FROM $tablename WHERE USER_ID = $user_id";

    if (!$isAll)
    {
        $sql = $sql . " AND TIMESTAMP > $lastUpdated";
    }

    $queryResult = mysql_query($sql);

    $rosters = array();
    $timestamp = time();

    while ($row = mysql_fetch_array($queryResult))
    {
        extract($row);

        $roster_id = $ROSTER_ID;

        //get roster details from rosters table
        $tablenameRosters = "rosters";
        $queryResultRosters = mysql_query("SELECT * FROM $tablenameRosters WHERE ROSTER_ID = $roster_id");

        $rosterDetails = array();
        while ($rowRoster = mysql_fetch_array($queryResultRosters))
        {
            extract($rowRoster);
            $rosterDetails["user_id"] = (Integer) $USER_ID;
            $rosterDetails["title"] = $TITLE;
            $rosterDetails["type"] = $TYPE;
            $rosterDetails["start_time"] = (Double) $START_TIME;
            $rosterDetails["end_time"] = (Double) $END_TIME;
            $rosterDetails["end_time_required"] = (Integer) $END_TIME_REQUIRED;
            $rosterDetails["timestamp"] = (Double) $TIMESTAMP;
        }

        $shiftCycles = array();

        $tablenameShiftCycles = "shiftCycles";
        $queryResultForShiftCycles = mysql_query("SELECT * FROM $tablenameShiftCycles WHERE ROSTER_ID = $roster_id ORDER BY INDEX_ID");

        while ($rowShiftCycles = mysql_fetch_array($queryResultForShiftCycles))
        {
            extract($rowShiftCycles);

            $aCycle = array("indexId" => (Integer) $INDEX_ID, "daysOn" => (Integer) $DAYS_ON, "daysOff" => (Integer) $DAYS_OFF, "timestamp" => (Double) $TIMESTAMP);

            array_push($shiftCycles, $aCycle);
        }

        $rosterDetails["shiftCycles"] = $shiftCycles;

        $rosters[$roster_id] = $rosterDetails;
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp,
        "isAll" => $isAll,
        "rosters" => $rosters);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}