<?php

/*
 * API: /user/friends/updateFriends.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "friends_id": [2, 3, 4, 5],
 *       "friends_color": [0x000000, 0x000000, 0x000000, 0x000000],
 *       "friends_selected": [0, 0, 0, 0]
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $friends_id = $input->friends_id;
    $friends_color = $input->friends_color;
    $friends_selected = $input->friends_selected;

    // check the email address in the users table
    $tablename = "friends";
    $timestamp = time();

    for ($i = 0; $i < count($friends_id); $i++)
    {
        $aFriend_id = $friends_id[$i];
        $aFriend_colour = $friends_color[$i];
        $aFriend_selected = $friends_selected[$i];

        $sql = "UPDATE $tablename SET FRIEND_COLOR = $aFriend_colour, FRIEND_SELECTED = $aFriend_selected, TIMESTAMP = $timestamp WHERE USER_ID = $user_id AND FRIEND_ID = $aFriend_id";
        $queryResult = mysql_query($sql);
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>