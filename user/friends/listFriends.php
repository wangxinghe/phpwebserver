<?php

/*
 * API: /user/friends/listFriends.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "lastUpdated":1447828080.00074
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *       "timestamp":1447828080.00074,
 *       "isAll": 0,
 *       "friends": {
 *             "1": {"friend_color": 0x00000, "friend_selected": 0},
 *             "2": {},
 *             "3": {},
 *             "4": {},
 *             "5": {}
 *       ]
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $lastUpdated = $input->lastUpdated;

    $isAll = (Integer) $lastUpdated < (Integer) User::lastFriendsOperationTimestamp($user_id);

    // check the email address in the users table
    $tablename = "friends";
    $sql = "SELECT DISTINCT FRIEND_ID, FRIEND_COLOR, FRIEND_SELECTED FROM $tablename WHERE USER_ID = $user_id";

    if (!$isAll)
    {
        $sql = $sql . " AND TIMESTAMP > $lastUpdated";
    }

    $queryResult = mysql_query($sql);

    $friends = array();
    $timestamp = time();

    while ($row = mysql_fetch_array($queryResult))
    {
        extract($row);

        $aFriend_id = (Integer) $FRIEND_ID;
        $friendDetails = array("friend_color" => (Integer) $FRIEND_COLOR, "friend_selected" => (Integer) $FRIEND_SELECTED);

        $friends[$aFriend_id] = $friendDetails;
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp,
        "isAll" => $isAll,
        "friends" => $friends);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>