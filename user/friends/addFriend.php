<?php

/*
 * API: /user/friends/addFriend.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "friend_id": 2
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/userShiftsState.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $friend_id = $input->friend_id;

    // check the email address in the users table
    $tablename = "friends";
    $timestamp = time();

    //insert new friendship

    mysql_query("DELETE FROM $tablename WHERE USER_ID = $user_id AND FRIEND_ID = $friend_id");
    $sql = "INSERT INTO $tablename (USER_ID, FRIEND_ID, FRIEND_COLOR, FRIEND_SELECTED, TIMESTAMP) VALUES ($user_id, $friend_id, 0, 0, $timestamp)";
    $queryResult = mysql_query($sql);

    UserShiftsState::update($user_id);

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>