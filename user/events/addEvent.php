<?php

/*
 * API: /user/events/addEvent.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "event":{  
 *          "title":"holiday",
 *          "location":"131 Queens Street",
 *          "friends":[  
 *              1,
 *              2,
 *              3
 *              ],
 *          "all_day":0,
 *          "start_time":1447828080.00074,
 *          "end_time":1447828080.00074,
 *          "repeat":0,
 *          "type":"Annual Leave",
 *          "alert":7200,
 *          "enabled":1
 *      }
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 *      "event_id": 1
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';
require_once $_SERVER["DOCUMENT_ROOT"] . '/model/event.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $event = new Event();
    $input_event = $input->event;

    $event->user_id = (Integer) $input->user_id;
    $event->title = $input_event->title;
    $event->location = $input_event->location;
    $event->friends = $input_event->friends;
    $event->all_day = (Integer) $input_event->all_day;
    $event->start_time = (Double) $input_event->start_time;
    $event->end_time = (Double) $input_event->end_time;
    $event->repeat = (Integer) $input_event->repeat;
    $event->type = $input_event->type;
    $event->alert = (Integer) $input_event->alert;
    $event->enabled = (Integer) $input_event->enabled;
    $event->timestamp = time();

    // check the email address in the users table
    $tablename = "events";

    $sql = "INSERT INTO $tablename (USER_ID, TITLE, LOCATION, ALL_DAY, START_TIME, END_TIME, REPEAT_, TYPE, ALERT, FRIENDS, ENABLED, TIMESTAMP) "
            . "VALUES ($event->user_id, '$event->title', '$event->location', $event->all_day, $event->start_time, $event->end_time, $event->repeat, '$event->type', $event->alert, '$event->friends', $event->enabled, $event->timestamp)";
    $queryResult = mysql_query($sql);

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $event->timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>