<?php

/*
 * API: /user/events/listEvents.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "lastUpdated":1447828080.00074
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *       "timestamp":1447828080.00074,
 *       "isAll":0,
 *       "events": {
 *             "1": {  
 *                      "title":"holiday",
 *                      "location":"131 Queens Street",
 *                      "friends":[  
 *                          1,
 *                          2,
 *                          3
 *                      ],
 *                      "all_day":0,
 *                      "start_time":1447828080.00074,
 *                      "end_time":1447828080.00074,
 *                      "repeat":0,
 *                      "type":"Annual Leave",
 *                      "alert":7200,
 *                      "enabled":1
 *                  },
 *             "2":{},
 *             "3":{},
 *             "4":{},
 *             "5":{}
 *       }
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = $input->user_id;
    $lastUpdated = $input->lastUpdated;

    $isAll = (Integer) $lastUpdated < (Integer) User::lastEventsOperationTimestamp($user_id);

    // check the email address in the users table
    $tablename = "events";
    $sql = "SELECT DISTINCT EVENT_ID FROM $tablename WHERE USER_ID = $user_id";

    if (!$isAll)
    {
        $sql = $sql . " AND TIMESTAMP > $lastUpdated";
    }

    $queryResult = mysql_query($sql);

    $events = array();
    $timestamp = time();

    while ($row = mysql_fetch_array($queryResult))
    {
        extract($row);

        $event_id = $EVENT_ID;

        $tablenameEvents = "events";
        $queryResultEvent = mysql_query("SELECT * FROM $tablenameEvents WHERE EVENT_ID = $event_id");

        $eventDetails = array();

        while ($rowEvent = mysql_fetch_array($queryResultEvent))
        {
            extract($rowEvent);

            $eventDetails["title"] = $TITLE;
            $eventDetails["location"] = $LOCATION;
            $eventDetails["friends"] = $FRIENDS;
            $eventDetails["all_day"] = (Integer) $ALL_DAY;
            $eventDetails["start_time"] = (Double) $START_TIME;
            $eventDetails["end_time"] = (Double) $END_TIME;
            $eventDetails["repeat"] = (Integer) $REPEAT_;
            $eventDetails["type"] = $TYPE;
            $eventDetails["alert"] = (Integer) $ALERT;
            $eventDetails["enabled"] = (Integer) $ENABLED;
            $eventDetails["timestamp"] = (Double) $TIMESTAMP;
        }

        $events[$event_id] = $eventDetails;
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp,
        "isAll" => $isAll,
        "events" => $events);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>