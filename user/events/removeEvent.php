<?php

/*
 * API: /user/events/removeEvent.php
 * 
 * Input: 
 * 
 * {
 *      "user_id": 1,
 *       "token": "123123123123123123123",
 *      "event_id": 0
 * }
 * 
 * Output:
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp": 1447828080.00074
 * }
 * 
 * */


require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $user_id = (Integer) $input->user_id;
    $event_id = (Integer) $input->event_id;

    // check the email address in the users table
    $tablename = "events";

    $sql = "DELETE FROM $tablename WHERE USER_ID = $user_id AND EVENT_ID = $event_id";
    $queryResult = mysql_query($sql);

    User::updateLastEventsOperationTimestamp($user_id);

    $timestamp = time();
    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>