<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$link = -1;
$serverIp = "127.0.0.1";
$databasename = "ShiftEzeDB";
$username = "root";
$password = "";

require_once $_SERVER["DOCUMENT_ROOT"] . '/model/user.php';

function generateRandomString($length = 20)
{
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++)
    {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

function openDB()
{
    global $link, $serverIp, $databasename, $username, $password;

    $link = mysql_connect($serverIp, $username, $password) or die('Cannot connect to the DB');
    mysql_select_db($databasename, $link) or die('Cannot select the DB');
}

function openDBAndValidate($user_id, $token)
{
    global $link, $serverIp, $databasename, $username, $password;

    $link = mysql_connect($serverIp, $username, $password) or die('Cannot connect to the DB');
    mysql_select_db($databasename, $link) or die('Cannot select the DB');

    if (!User::isValidToken($user_id, $token))
    {
        closeDB();

        $result = array("status" => 1,
            "message" => "Invalid token!",
            "timestamp" => time());

        header('Content-type: application/json');
        echo(json_encode($result));

        exit();
    }
}

function closeDB()
{
    global $link;
    if ($link != -1)
    {
        mysql_close($link);
    }
}

function showMenu($menu)
{
    echo "<ol>$menu</ol>";
}

function showItem($item)
{
    echo "<li>$item</li>";
}

function showResult($cmd, $tablename, $text)
{
    echo("<h3>$cmd $tablename: $text</h3>");
}
