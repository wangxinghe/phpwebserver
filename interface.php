<?php

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

$alltables = array("users", "users_settings", "friends", "events", "shifts", "user_shifts_states", "rosters", "shiftCycles", "friends_operation_status", "events_operation_status", "rosters_operation_status", "shifts_operation_status");

if (isset($_GET['op']))
{
    openDB();

    $op = $_GET['op'];

    if ($op == "create")
    {
        build();
    } else if ($op == "clean")
    {
        clean();
    } else if ($op == "drop")
    {
        drop();
    } else if ($op == "rebuild")
    {
        drop();
        build();
    } else
    {
        show('unknow command!');
        showCmd();
    }

    closeDB();
} else
{
    showCmd();
}

function showCmd()
{
    show("op=create");
    show("op=clean");
    show("op=drop");
    show("op=rebuild");
}

function show($msg)
{
    echo("<h3>$msg</h3>");
}

function drop()
{
    global $alltables;

    foreach ($alltables as $tablename)
    {
        $SQL = "DROP TABLE $tablename";
        showResult("Drop", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");
    }
}

function build()
{
    $textLength = 100;
    $tablename = "users";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (USER_ID INTEGER AUTO_INCREMENT PRIMARY KEY, FIRST_NAME CHAR($textLength), LAST_NAME CHAR($textLength), EMAIL CHAR($textLength), PASSWORD CHAR($textLength), TEL CHAR($textLength), LOCATION CHAR($textLength), PROFILE MEDIUMTEXT, IS_FACEBOOK INTEGER, CURRENT_STATE INTEGER, TOKEN CHAR($textLength), TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "users_settings";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, SETTINGS CHAR($textLength), TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "friends";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, FRIEND_ID INTEGER, FRIEND_COLOR INTEGER, FRIEND_SELECTED INTEGER, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "events";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (EVENT_ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, TITLE CHAR($textLength), LOCATION CHAR($textLength), ALL_DAY INTEGER, START_TIME DOUBLE, END_TIME DOUBLE, REPEAT_ INTEGER, TYPE CHAR($textLength), ALERT INTEGER, FRIENDS CHAR($textLength), ENABLED INTEGER, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "shifts";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (SHIFT_ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, ROSTER_ID INTEGER, TITLE CHAR($textLength), TYPE CHAR($textLength), DESCRIPTION CHAR($textLength), START_TIME DOUBLE, END_TIME DOUBLE, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "user_shifts_states";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "rosters";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (ROSTER_ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, TITLE CHAR($textLength), TYPE CHAR($textLength), START_TIME DOUBLE, END_TIME DOUBLE, END_TIME_REQUIRED INTEGER, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "shiftCycles";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (ID INTEGER AUTO_INCREMENT PRIMARY KEY, ROSTER_ID INTEGER, INDEX_ID INTEGER, DAYS_ON INTEGER, DAYS_OFF INTEGER, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "friends_operation_status";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "events_operation_status";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "rosters_operation_status";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");

    $tablename = "shifts_operation_status";
    $SQL = "CREATE TABLE IF NOT EXISTS $tablename (ID INTEGER AUTO_INCREMENT PRIMARY KEY, USER_ID INTEGER, TIMESTAMP DOUBLE)";
    showResult("Create", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");
}

function clean()
{
    global $alltables;

    foreach ($alltables as $tablename)
    {
        $SQL = "DELETE FROM $tablename";
        showResult("Clean", $tablename, mysql_query($SQL) == 1 ? "OK" : "ERROR");
    }
}

?>
