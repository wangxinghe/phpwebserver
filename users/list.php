<?php

/*
 * API: /users/list.php
 * 
 * Input:
 * 
 * {
 *       "user_id": 1,
 *       "token": "123123123123123123123",
 *       "lastUpdated":1447828080.00074
 * }
 * 
 * Output:  
 * 
 * {
 *       "status": 0,
 *       "message": "",
 *      "timestamp":1447828080.00074,
 *       "users": {
 *             "1": {
 *                  "first_name":"Ricol",
 *                  "last_name":"Wang",
 *                  "email":"ricol.wang@appscore.com.au",
 *                  "tel":"0423584908",
 *                  "location":"51 Cardigan Carlton 3053 VIC",
 *                  "isFacebook":0,
 *                  "timestamp": 1447828080.00074
 *              },
 *             "2": {},
 *             "3": {},
 *             "4": {},
 *             "5": {}
 *       }
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST) || $_SERVER["REQUEST_METHOD"] == "GET")
{
    $input = json_decode($HTTP_RAW_POST_DATA);
    openDBAndValidate($input->user_id, $input->token);

    $lastUpdated = $input->lastUpdated;

    $tablename = "users";
    $sql = "SELECT DISTINCT USER_ID FROM $tablename WHERE TIMESTAMP > $lastUpdated";
    $queryResult = mysql_query($sql);

    $users = array();
    $timestamp = time();

    while ($row = mysql_fetch_array($queryResult))
    {
        extract($row);

        $userId = $USER_ID;

        $userTable = "users";
        $details = array();
        $queryResultFromUser = mysql_query("SELECT * FROM $userTable WHERE USER_ID = $userId");

        while ($rowUser = mysql_fetch_array($queryResultFromUser))
        {
            extract($rowUser);

            $details["user_id"] = (Integer) $USER_ID;
            $details["first_name"] = $FIRST_NAME;
            $details["last_name"] = $LAST_NAME;
            $details["email"] = $EMAIL;
            $details["tel"] = $TEL;
            $details["location"] = $LOCATION;
            $details["profile"] = $PROFILE;
            $details["isFacebook"] = (Integer) $IS_FACEBOOK;
            $details["timestamp"] = (Double) $TIMESTAMP;
            break;
        }

        $users[$USER_ID] = $details;
    }

    $result = array("status" => 0,
        "message" => "",
        "timestamp" => $timestamp,
        "users" => $users);

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>