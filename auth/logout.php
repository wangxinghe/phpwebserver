<?php

/*
 * API: /auth/logout.php
 * 
 * Input:
 * 
 * {
 *       "user_email":"ricol.wang@appscore.com.au",
 *       "token":"!@#$%^&*()"
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    $result = array("status" => 0,
        "message" => "",
        "timestamp" => time());

    header('Content-type: application/json');
    echo(json_encode($result));
}
?>