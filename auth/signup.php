<?php

/*
 * API: /auth/signup.php
 * 
 * Input:
 * 
 * {
 *       "first_name":"Ricol",
 *       "last_name":"Wang",
 *       "email":"ricol.wang@appscore.com.au",
 *       "password":"star1983",
 *       "tel":"0423584908",
 *       "location":"51 Cardigan Carlton 3053 VIC"
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    openDB();

    $input = json_decode($HTTP_RAW_POST_DATA);

    $first_name = $input->first_name;
    $last_name = $input->last_name;
    $email = $input->email;
    $password = $input->password;
    $tel = $input->tel;
    $location = $input->location;

    // check the email address in the users table
    $tablename = "users";
    $sql = "SELECT COUNT(*) AS COUNT FROM $tablename WHERE EMAIL = '$email'";

    $queryResult = mysql_query($sql);
    $count = 0;
    while ($row = mysql_fetch_row($queryResult))
    {
        $count = $row[0];
    }

    $result = array();
    $timestamp = time();

    if ($count > 0)
    {
        //if exists, return error
        $result = array("status" => -1,
            "message" => "email already registered!",
            "timestamp" => $timestamp);
    } else
    {
        //if doesn't exist, return succeed
        $sql = "INSERT INTO $tablename "
                . "(FIRST_NAME, LAST_NAME, EMAIL, PASSWORD, TEL, LOCATION, PROFILE, IS_FACEBOOK, CURRENT_STATE, TOKEN, TIMESTAMP) VALUES "
                . "('$first_name', '$last_name', '$email', '$password', '$tel', '$location', '', 0, 0, '', $timestamp)";
        $queryResult = mysql_query($sql);
        if ($queryResult)
        {
            $sql = "SELECT USER_ID FROM $tablename WHERE EMAIL = '$email'";
            $queryResult = mysql_query($sql);

            $user_id = -1;
            while ($row = mysql_fetch_array($queryResult))
            {
                extract($row);
                $user_id = $USER_ID;
            }

            $result = array("status" => 0,
                "message" => "",
                "user_id" => (Integer) $user_id,
                "timestamp" => $timestamp);
        } else
        {
            $result = array("status" => -1,
                "message" => "database operation failed!",
                "timestamp" => $timestamp);
        }
    }

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>