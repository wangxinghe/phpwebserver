<?php

/*
 * API: /auth/login.php
 * 
 * Input:
 * 
 * {
 *       "user_email":"ricol.wang@appscore.com.au",
 *       "user_password":"star1983"
 * }
 * 
 * Output:  
 * 
 * {
 *      "status": 0,
 *      "message": "",
 *      "timestamp":1447828080.00074,
 *      "user_id":1,
 *      "token":"!@#$%^&*()"
 * }
 * 
 * */

require_once $_SERVER["DOCUMENT_ROOT"] . '/common.php';

if ($_SERVER['REQUEST_METHOD'] == "POST" && isset($_POST))
{
    openDB();

    $input = json_decode($HTTP_RAW_POST_DATA);

    $user_email = $input->user_email;
    $user_password = $input->user_password;

    // check the email address in the users table
    $tablename = "users";
    $sql = "SELECT COUNT(*) AS COUNT FROM $tablename WHERE EMAIL = '$user_email'";

    $queryResult = mysql_query($sql);
    $count = 0;
    while ($row = mysql_fetch_row($queryResult))
    {
        $count = $row[0];
    }

    $result = array();
    $timestamp = time();

    if ($count <= 0)
    {
        //if doesn't exist, return error
        $result = array("status" => -1,
            "message" => "email not registered!",
            "timestamp" => $timestamp);
    } else
    {
        //if exist, return succeed
        //check password
        $sql = "SELECT PASSWORD FROM $tablename WHERE EMAIL = '$user_email'";
        $queryResult = mysql_query($sql);

        $password = '';
        while ($row = mysql_fetch_array($queryResult))
        {
            extract($row);
            $password = $PASSWORD;
        }
        //if correct
        if ($user_password == $password)
        {
            $token = generateRandomString();

            $sql = "UPDATE $tablename "
                    . "SET CURRENT_STATE = 1, TOKEN = '$token' "
                    . "WHERE EMAIL = '$user_email'";
            $queryResult = mysql_query($sql);
            if ($queryResult)
            {
                $sql = "SELECT USER_ID FROM $tablename WHERE EMAIL = '$user_email'";
                $queryResult = mysql_query($sql);

                $user_id = -1;
                while ($row = mysql_fetch_array($queryResult))
                {
                    extract($row);
                    $user_id = $USER_ID;
                }

                $result = array("status" => 0,
                    "message" => "",
                    "timestamp" => $timestamp,
                    "user_id" => (Integer) $user_id,
                    "token" => $token);
            } else
            {
                $result = array("status" => -1,
                    "message" => "database operation failed!",
                    "timestamp" => $timestamp);
            }
        } else
        {
            //if not correct
            $result = array("status" => -1,
                "message" => "password not correct",
                "timestamp" => $timestamp);
        }
    }

    header('Content-type: application/json');
    echo(json_encode($result));

    closeDB();
}
?>